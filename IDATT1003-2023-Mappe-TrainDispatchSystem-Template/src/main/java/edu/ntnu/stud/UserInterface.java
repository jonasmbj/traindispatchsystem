package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * UserInterface class manages the user interaction for train departures.
 */
public class UserInterface {
  // Instance variables
  private TrainDepartureRegister trainDepartureRegister;
  private InputValidator validator;
  private ArrayList<TrainDeparture> trainDepartures;
  Scanner in = new Scanner(System.in);
  LocalTime currentTime = LocalTime.of(0, 0);

  /**
   * Initializes the UserInterface with a TrainDepartureRegister.
   *
   * @param trainDepartureRegister The TrainDepartureRegister instance to be initialized.
   */
  public void init(TrainDepartureRegister trainDepartureRegister) {
    // Initialize instance variables
    this.trainDepartureRegister = trainDepartureRegister;
    this.validator = new InputValidator();
    trainDepartures = new ArrayList<>();
    testData();
  }

  /**
   * Starts the application by calling the method to display the menu.
   */
  public void start() {
    menu();
  }

  /**
   * Displays a menu for managing train departures, allowing users to perform various actions.
   * The menu includes options to register, print, delete, delay, search, change, set time, or exit.
   * The user's choice determines the action taken using a switch statement:
   * - 1: Register a new train departure.
   * - 2: Print all train departures.
   * - 3: Delete a train departure.
   * - 4: Add a delay to a train departure.
   * - 5: Search for a train departure.
   * - 6: Change an element for a specific train departure.
   * - 7: Set the time.
   * - 10: Exit the menu and terminate the program.
   * Handles InputMismatchException by displaying an error message and recursively calling itself.
   */
  public void menu() {
    try {
      // Menu display and user input handling
      boolean running = true;
      while (running) {
        System.out.println();
        System.out.println("The current time is " + currentTime);
        System.out.println("Press 1 to register a new train departure");
        System.out.println("Press 2 to print all train departures");
        System.out.println("Press 3 to delete a train departure");
        System.out.println("Press 4 to add a delay to a train departure");
        System.out.println("Press 5 to search for a train departure");
        System.out.println("Press 6 to change an element for a specific train departure");
        System.out.println("Press 7 to set the time");
        System.out.println("Press 10 to exit");
        int choice = in.nextInt();

        // Perform actions based on user input
        switch (choice) {
          case 1 -> caseRegister(in);
          case 2 -> trainDepartureRegister.printAllTrainDepartures(currentTime);
          case 3 -> caseDelete(in);
          case 4 -> caseDelay(in);
          case 5 -> caseSearch(in);
          case 6 -> caseChange(in);
          case 7 -> caseTime(in);
          case 10 -> running = false;
          default -> System.out.println("Invalid input! You must enter a number between 1 and 10");
        }
      }
    } catch (InputMismatchException ime) {
      // Handle invalid input
      System.out.println("Invalid input!");
      in.nextLine();
      menu();
    }
  }

  /**
   * Registers a new train departure by prompting the user for departure details.
   *
   * @param in The Scanner object used for user input.
   */
  public void caseRegister(Scanner in) {
    // Prompt user for departure details
    boolean validInput = false;
    in.nextLine();
    while (!validInput) {
      try {
        boolean validTime = false;
        LocalTime parsedTime = null;
        while (!validTime) {
          // Prompt user for departure time
          System.out.println("Enter the departure time (HH:mm):");
          String departureTime = in.nextLine();
          if (InputValidator.isTime(departureTime)) {
            parsedTime = LocalTime.parse(departureTime);
            validTime = true;
          } else {
            // Handle invalid time input
            System.out.println("Invalid time format. Please enter the time in HH:mm format.");
          }
        }

        System.out.println("Enter the train line:");
        final String line = in.nextLine();

        int trainNumber = 0;
        boolean validTrainNumber = false;
        while (!validTrainNumber) {
          // Prompt user for train number
          System.out.println("Enter the train number:");
          if (in.hasNextInt()) {
            trainNumber = in.nextInt();
            if (trainDepartureRegister.searchForTrainNumber(trainNumber)) {
              System.out.println("This train number already exists."
                  + " Please enter a new train number:");
              continue;
            }
            in.nextLine(); // Consume newline character after nextInt()
            validTrainNumber = true;
          } else {
            // Handle invalid train number input
            System.out.println("Invalid input. Please enter a valid integer for the train number:");
            in.nextLine(); // Consume the invalid input
          }
        }

        System.out.println("Enter the destination:");
        String destination = in.nextLine();

        boolean validTrack = false;
        String track = "";
        while (!validTrack) {
          System.out.println("Enter the track number:");
          track = in.nextLine();
          if (InputValidator.isTrack(track)) {
            validTrack = true;
          } else {
            System.out.println("Invalid input. Please enter a valid track number:");
          }
        }

        LocalTime delay = LocalTime.of(0, 0);

        trainDepartureRegister.addTrainDeparture(
            parsedTime, line, trainNumber, destination, track, delay);
        validInput = true;
      } catch (DateTimeParseException | InputMismatchException e) {
        System.out.println("Invalid input. Please enter the correct format.");
        System.out.println("Restarting entry process...");
      }
    }
  }

  /**
   * Deletes a train departure based on the provided train number.
   *
   * @param in The Scanner object used for user input.
   */
  public void caseDelete(Scanner in) {
    boolean validInput = false;
    while (!validInput) {
      try {
        System.out.println("Enter the train number:");
        int trainNumber = in.nextInt();
        if (trainDepartureRegister.searchForTrainNumber(trainNumber)) {
          System.out.println("The train departure has been deleted");
          trainDepartureRegister.deleteTrainDeparture(trainNumber);
          validInput = true;
        } else {
          System.out.println("This train number does not exist");
        }
      } catch (InputMismatchException ime) {
        System.out.println("Invalid input!");
        in.nextLine();
      }
    }
  }

  /**
   * Searches for a train departure based on the provided train number or destination.
   *
   * @param in The Scanner object used for user input.
   */
  public void caseSearch(Scanner in) {
    boolean validInput = false;
    while (!validInput) {
      try {
        System.out.println("Press 1 to search for a train departure by train number");
        System.out.println("Press 2 to search for a train departure by destination");
        System.out.println("Press 3 to exit");
        int choice2 = in.nextInt();
        switch (choice2) {
          case 1 -> {
            boolean validInput2 = false;
            while (!validInput2) {
              try {
                System.out.println("Enter the train number");
                int trainNumber = in.nextInt();
                if (trainDepartureRegister.searchForTrainNumber(trainNumber)) {
                  validInput2 = true;
                } else {
                  System.out.println("This train departure does not exist");
                }
              } catch (InputMismatchException ime) {
                System.out.println("Invalid input!");
                in.nextLine();
              }
            }
          }
          case 2 -> {
            in.nextLine();
            boolean validInput3 = false;
            while (!validInput3) {
              try {
                System.out.println("Enter the destination");
                String destination = in.nextLine();
                if (trainDepartureRegister.searchForDestination(destination)) {
                  validInput3 = true;
                } else {
                  System.out.println("This train departure does not exist");
                }
              } catch (InputMismatchException ime) {
                System.out.println("Invalid input!");
                in.nextLine();
              }
            }
          }
          case 3 -> {
            validInput = true;
            break;
          }
          default -> {
            System.out.println("Invalid input");
            caseSearch(in);
          }
        }
      } catch (InputMismatchException ime) {
        System.out.println("Invalid input!");
        in.nextLine();
      }
    }
  }

  /**
   * Adds a delay to a train departure based on the provided train number.
   *
   * @param in The Scanner object used for user input.
   */
  public void caseDelay(Scanner in) {
    boolean validInput = false;
    in.nextLine();
    while (!validInput) {
      try {
        System.out.println("Enter the train number:");
        int trainNumber = in.nextInt();

        if (InputValidator.isInteger(trainNumber)) {
          if (!trainDepartureRegister.searchForTrainNumber(trainNumber)) {
            System.out.println("This train number does not exist");
          } else {
            trainDepartureRegister.addDelay(trainNumber, in);
            System.out.println("The delay has been updated");
            validInput = true;
          }
        } else {
          System.out.println("Invalid input! Please enter a valid train number");
        }
      } catch (InputMismatchException ime) {
        System.out.println("Invalid input!");
        in.nextLine();
      }
    }
  }

  /**
   * Changes an element for a specific train departure based on the provided train number.
   *
   * @param in The Scanner object used for user input.
   */
  public void caseChange(Scanner in) {
    boolean validInput = false;
    while (!validInput) {
      try {
        System.out.println("Enter the train number of the departure you want to change:");
        int trainNumber = in.nextInt();

        if (!InputValidator.isInteger(trainNumber)) {
          System.out.println("Invalid input! Please enter a valid train number.");
          continue;
        }

        if (!trainDepartureRegister.searchForTrainNumber(trainNumber)) {
          System.out.println("This train number does not exist");
          continue;
        }
        while (!validInput) {
          try {
            System.out.println();
            System.out.println("Press 1 to change the departure time");
            System.out.println("Press 2 to change the train line");
            System.out.println("Press 3 to change the destination");
            System.out.println("Press 4 to change the track number");
            System.out.println("Press 5 to exit");
            int choice = in.nextInt();

            if (!InputValidator.isInteger(choice)) {
              System.out.println("Invalid input! Please enter a valid choice.");
              continue;
            }
            switch (choice) {
              case 1 -> {
                in.nextLine();
                trainDepartureRegister.changeDepartureTime(trainNumber, in);
              }
              case 2 -> {
                in.nextLine();
                trainDepartureRegister.changeLine(trainNumber, in);
              }
              case 3 -> {
                in.nextLine();
                trainDepartureRegister.changeDestination(trainNumber, in);
              }
              case 4 -> {
                in.nextLine();
                trainDepartureRegister.changeTracks(trainNumber, in);
              }
              case 5 -> {
                validInput = true;
                break;
              }
              default -> System.out.println("Invalid input");
            }
          } catch (InputMismatchException ime) {
            System.out.println("Invalid input!");
            in.nextLine();
          }
        }
      } catch (InputMismatchException ime) {
        System.out.println("Invalid input!");
        in.nextLine();
      }
    }
  }

  /**
   * Sets the time to be used for train departures.
   *
   * @param in The Scanner object used for user input.
   */
  public void caseTime(Scanner in) {
    boolean validInput = false;
    in.nextLine();
    while (!validInput) {
      System.out.println("Enter the time (HH:mm):");
      String time = in.nextLine();

      if (InputValidator.isTime(time)) {
        try {
          LocalTime timeCheck = LocalTime.parse(time);
          if (timeCheck.isBefore(currentTime)) {
            System.out.println("The time cannot be before the current time");
            continue;
          }
          currentTime = timeCheck;
          validInput = true;
          trainDepartureRegister.deleteDeparturesBeforeTime(currentTime);
        } catch (DateTimeParseException e) {
          System.out.println("Error parsing time.");
        }
      } else {
        System.out.println("Invalid time format. Please enter the time in HH:mm format.");
      }
    }
  }

  /**
   * Adds some test data to the TrainDepartureRegister.
   */
  public void testData() {
    trainDepartureRegister.addTrainDeparture(LocalTime.of(23, 16), "L1", 123, "Spikkestad", "4",
        LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(LocalTime.of(13, 18), "L1", 124, "Lillestrøm", "6",
        LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(LocalTime.of(13, 19), "L13", 125, "Dal", "3",
        LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(LocalTime.of(13, 29), "L12", 126, "Eidsvoll", "3",
        LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(LocalTime.of(13, 30), "L12", 127, "Kongsberg", "1",
        LocalTime.of(0, 0));
  }

}