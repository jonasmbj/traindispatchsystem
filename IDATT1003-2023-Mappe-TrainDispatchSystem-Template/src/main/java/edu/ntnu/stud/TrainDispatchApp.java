package edu.ntnu.stud;

/**
 * The main class representing the Train Dispatch Application.
 */
public class TrainDispatchApp {

  /**
   * The main entry point of the Train Dispatch Application.
   *
   * @param args Command-line arguments (unused)
   */
  public static void main(String[] args) {
    // Initialize the UserInterface and TrainDepartureRegister
    UserInterface ui = new UserInterface();
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();

    // Initialize UserInterface with TrainDepartureRegister and start the application
    ui.init(trainDepartureRegister);
    ui.start();
  }
}
