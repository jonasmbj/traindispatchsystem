package edu.ntnu.stud;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.Test;

/**
 * The test in this class have taken inspiration from a test made by GitHub Copilot:
 */
public class TrainDepartureTest {

  /**
   * Test case to verify TrainDeparture constructor with valid inputs.
   */
  @Test
  public void testTrainDepartureConstructor_ValidInputs() {
    // Arrange: Create input values for the TrainDeparture constructor
    LocalTime departureTime = LocalTime.of(9, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(0, 0);

    // Act: Create a TrainDeparture object using the constructor
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Assert: Verify that the object is constructed correctly
    assertEquals(departureTime, trainDeparture.getDepartureTime());
    assertEquals(line, trainDeparture.getLine());
    assertEquals(trainNumber, trainDeparture.getTrainNumber());
    assertEquals(destination, trainDeparture.getDestination());
    assertEquals(track, trainDeparture.getTrack()); // Ensure the default value for an empty track is set to "-1"
    assertEquals(delay, trainDeparture.getDelay());
  }

  /**
   * Test case to verify TrainDeparture constructor with invalid inputs.
   */
  @Test
  public void testTrainDepartureConstructor_InvalidInputs() {
    // Arrange: Create input values for the TrainDeparture constructor
    LocalTime departureTime = LocalTime.of(9, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "";
    LocalTime delay = LocalTime.of(0, 0);

    // Act: Create a TrainDeparture object using the constructor
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Assert: Verify that the object is constructed correctly
    assertNotEquals(LocalTime.of(0, 0), trainDeparture.getDepartureTime());
    assertEquals(LocalTime.of(9, 0), trainDeparture.getOriginalDepartureTime());
    assertNotEquals("L2", trainDeparture.getLine());
    assertNotEquals(123, trainDeparture.getTrainNumber());
    assertNotEquals("Destination Y", trainDeparture.getDestination());
    assertEquals("-1", trainDeparture.getTrack()); // Ensure the default value for an empty track is set to "-1"
    assertNotEquals(LocalTime.of(12, 0), trainDeparture.getDelay());
  }

  /**
   * Test case to calculate midnight checker for a TrainDeparture instance with past midnight times.
   */
  @Test
  public void testGetMidnightChecker_PastMidnight() {
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(20, 30); // 1 hour and 30 minutes delay
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Calculate expected midnight checker value manually
    long expectedMidnightChecker = ChronoUnit.SECONDS.between(LocalTime.MIDNIGHT, departureTime) +
        ChronoUnit.SECONDS.between(LocalTime.MIDNIGHT, delay);

    // Act: Call the getMidnightChecker() method
    long actualMidnightChecker = trainDeparture.getMidnightChecker();

    // Assert: Verify that the calculated midnight checker matches the expected value
    assertEquals(expectedMidnightChecker, actualMidnightChecker);
  }

  /**
   * Test case to calculate midnight checker for a TrainDeparture instance with time before midnight.
   */
  @Test
  public void testGetMidnightChecker_BeforeMidnight() {
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(0, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Calculate expected midnight checker value manually
    long expectedMidnightChecker = ChronoUnit.SECONDS.between(LocalTime.MIDNIGHT, departureTime) +
        ChronoUnit.SECONDS.between(LocalTime.MIDNIGHT, delay);

    // Act: Call the getMidnightChecker() method
    long actualMidnightChecker = trainDeparture.getMidnightChecker();

    // Assert: Verify that the calculated midnight checker matches the expected value
    assertEquals(expectedMidnightChecker, actualMidnightChecker);
  }

  /**
   * Test case to verify setters in TrainDeparture class.
   */
  @Test
  public void testSetters(){
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(1, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Act: Call the setters to modify TrainDeparture properties
    trainDeparture.setDepartureTime(LocalTime.of(9,0));
    trainDeparture.setLine("L2");
    trainDeparture.setDestination("Destination Y");
    trainDeparture.setTrack("");
    trainDeparture.setDelay(LocalTime.of(12,0));

    // Assert: Verify that the properties were properly set
    assertEquals(LocalTime.of(9,0), trainDeparture.getOriginalDepartureTime());
    assertEquals("L2", trainDeparture.getLine());
    assertEquals("Destination Y", trainDeparture.getDestination());
    assertEquals("-1", trainDeparture.getTrack());
    assertEquals(LocalTime.of(12,0), trainDeparture.getDelay());
  }

  /**
   * Test case to verify setters in TrainDeparture class with negative assertions.
   */
  @Test
  public void testSetters_Negative(){
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(1, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Act: Call the setters to modify TrainDeparture properties
    trainDeparture.setDepartureTime(LocalTime.of(9,0));
    trainDeparture.setLine("L2");
    trainDeparture.setDestination("Destination Y");
    trainDeparture.setTrack("");
    trainDeparture.setDelay(LocalTime.of(12,0));

    // Assert: Verify that the properties were not set as expected
    assertNotEquals(LocalTime.of(8,0), trainDeparture.getOriginalDepartureTime());
    assertNotEquals("L1", trainDeparture.getLine());
    assertNotEquals("Destination X", trainDeparture.getDestination());
    assertNotEquals("3", trainDeparture.getTrack());
    assertNotEquals(LocalTime.of(1,0), trainDeparture.getDelay());
  }

  /**
   * Test case to validate the output of toString() with all attributes set.
   */
  @Test
  public void testToString(){
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(1, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Act: Call the toString() method
    String expectedString = "Departure time: " + trainDeparture.getDepartureTime() + ", Line: " + trainDeparture.getLine() + ", Train number: " + trainDeparture.getTrainNumber() + ", Destination: " + trainDeparture.getDestination() + ", Track: " + trainDeparture.getTrack() + ", Delay: " + trainDeparture.getDelay();
    String actualString = trainDeparture.toString();

    // Assert: Verify the output matches the expected string
    assertEquals(expectedString, actualString);
  }

  /**
   * Test case to validate the output of toString() when track and delay are empty.
   */
  @Test
  public void testToString_NoTrackOrDelay(){
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "";
    LocalTime delay = LocalTime.of(0, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Act: Call the toString() method
    String expectedString = "Departure time: " + trainDeparture.getDepartureTime() + ", Line: " + trainDeparture.getLine() + ", Train number: " + trainDeparture.getTrainNumber() + ", Destination: " + trainDeparture.getDestination();
    String actualString = trainDeparture.toString();

    // Assert: Verify the output matches the expected string
    assertEquals(expectedString, actualString);
  }

  /**
   * Test case to validate the output of toString() when track is empty.
   */
  @Test
  public void testToString_NoTrack(){
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "";
    LocalTime delay = LocalTime.of(1, 0);
    TrainDeparture trainDeparture = new TrainDeparture(departureTime, line, trainNumber, destination, track, delay);

    // Act: Call the toString() method
    String expectedString = "Departure time: " + trainDeparture.getDepartureTime() + ", Line: " + trainDeparture.getLine() + ", Train number: " + trainDeparture.getTrainNumber() + ", Destination: " + trainDeparture.getDestination() + ", Delay: " + trainDeparture.getDelay();
    String actualString = trainDeparture.toString();

    // Assert: Verify the output matches the expected string
    assertEquals(expectedString, actualString);
  }

    /**
     * Test case to validate the output of toString() when no attributes are empty.
     */
  @Test
  public void testToString_Negative() {
    // Arrange: Create a TrainDeparture object with specific departureTime and delay
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "L1";
    int trainNumber = 543;
    String destination = "Destination X";
    String track = "3";
    LocalTime delay = LocalTime.of(1, 0);

    // Simulate a scenario where destination is null or empty
    String nullDestination = null;
    String emptyTrack = "";

    // Create TrainDeparture objects with one attribute as null or empty
    TrainDeparture trainDepartureNullDestination = new TrainDeparture(departureTime, line, trainNumber, nullDestination, track, delay);
    TrainDeparture trainDepartureEmptyTrack = new TrainDeparture(departureTime, line, trainNumber, destination, emptyTrack, delay);

    // Act: Call the toString() method
    String actualStringNullDestination = trainDepartureNullDestination.toString();
    String actualStringEmptyTrack = trainDepartureEmptyTrack.toString();

    // Assert: Verify that the toString() method handles null or empty attributes properly
    // Adjust the assertions according to the expected behavior when attributes are null or empty
    // For instance, you might expect specific placeholders or messages instead of null/empty attributes
    assertTrue(actualStringNullDestination.contains("null")); // Example assertion, modify based on expected handling of null values
    assertTrue(actualStringEmptyTrack.contains("")); // Example assertion, modify based on expected handling of empty values
  }

}
