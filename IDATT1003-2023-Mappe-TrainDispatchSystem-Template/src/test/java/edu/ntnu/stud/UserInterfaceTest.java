package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * The test in this class have taken inspiration from a test made by GitHub Copilot:
 */
public class UserInterfaceTest {
  private UserInterface userInterface;
  private TrainDepartureRegister trainDepartureRegister;
  private ArrayList<TrainDeparture> trainDepartures;

  /**
   * Sets up the necessary objects before each test case.
   */
  @BeforeEach // This method is called before each test case
  public void setUp() {
    trainDepartureRegister = new TrainDepartureRegister();
    userInterface = new UserInterface();
    userInterface.init(trainDepartureRegister);
  }

  /**
   * Test case to verify positive scenario of registering a train departure.
   */
  @Test
  public void testCaseRegister_Positive() {
    // Set up input for the caseRegister method
    String mockInput = System.lineSeparator() + "10:00" + System.lineSeparator() + "L1"
        + System.lineSeparator() + "128" + System.lineSeparator() + "Oslo"
        + System.lineSeparator() + "1" + System.lineSeparator() + "00:00";
    Scanner mockedScanner = new Scanner(mockInput);

    // Call the caseRegister method
    userInterface.caseRegister(mockedScanner);

    // Verify if the train departure was added successfully
    assertTrue(trainDepartureRegister.searchForTrainNumber(128));
  }

  /**
   * Test case to verify negative scenario of registering a train departure.
   */
  @Test
  public void testCaseRegister_Negative() {
    // Set up input for the caseRegister method
    String mockInput = System.lineSeparator() + "10:00" + System.lineSeparator() + "L1"
        + System.lineSeparator() + "129" + System.lineSeparator() + "Oslo"
        + System.lineSeparator() + "1" + System.lineSeparator() + "00:00";
    Scanner mockedScanner = new Scanner(mockInput);

    // Call the caseRegister method
    userInterface.caseRegister(mockedScanner);

    // Verify if the train departure was added successfully
    assertFalse(trainDepartureRegister.searchForTrainNumber(120));
  }

  /**
   * Test case to verify positive deletion of a train.
   */
  @Test
  public void testDeleteTrain_Positive() {
    // Set up input for the caseDeleteTrain method
    String mockInput = "123";
    Scanner mockedScanner = new Scanner(mockInput);

    // Call the caseDeleteTrain method
    userInterface.caseDelete(mockedScanner);


    // Verify if the train departure was deleted successfully
    assertFalse(trainDepartureRegister.searchForTrainNumber(123));
  }

  /**
   * Test case to verify negative deletion of a train.
   */
  @Test
  public void testDeleteTrain_Negative() {
    // Set up input for the caseDeleteTrain method
    String mockInput = "123";
    Scanner mockedScanner = new Scanner(mockInput);

    // Call the caseDeleteTrain method
    userInterface.caseDelete(mockedScanner);


    // Verify if the train departure was deleted successfully
    assertTrue(trainDepartureRegister.searchForTrainNumber(124));
  }

  /**
   * Test case to verify delay for a specific train.
   */
  @Test
  public void testCaseDelay() {
    String mockInput = System.lineSeparator() + "123" + System.lineSeparator() + "10:00";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseDelay(mockScanner);

    assertEquals(LocalTime.of(10, 0), trainDepartureRegister.getDelay(123));
  }

  /**
   * Test case to verify delay for a different train.
   */
  @Test
  public void testCaseDelay_DifferentTrain() {
    String mockInput = System.lineSeparator() + "123" + System.lineSeparator() + "10:00";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseDelay(mockScanner);

    assertNotEquals(LocalTime.of(10, 0), trainDepartureRegister.getDelay(124));
  }

  /**
   * Test case to search for a specific train number.
   */
  @Test
  public void testCaseSearch_TrainNumber() {
    String mockInput = System.lineSeparator() + "1" + System.lineSeparator() + "123" + System.lineSeparator() + "3";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseSearch(mockScanner);

    assertTrue(trainDepartureRegister.searchForTrainNumber(123));
  }

  /**
   * Test case to search for a destination with lowercase input.
   */
  @Test
  public void testCaseSearch_Destination() {
    String mockInput = System.lineSeparator() + "2" + System.lineSeparator() + "Spikkestad" + System.lineSeparator() + "3";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseSearch(mockScanner);

    assertTrue(trainDepartureRegister.searchForDestination("Spikkestad"));
  }

  /**
   * Test case to search for a destination with capitalized input.
   */
  @Test
  public void testCaseSearch_DestinationCapitalization() {
    String mockInput = System.lineSeparator() + "2" + System.lineSeparator() + "SPIKKESTAD" + System.lineSeparator() + "3";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseSearch(mockScanner);

    assertTrue(trainDepartureRegister.searchForDestination("Spikkestad"));
  }

  /**
   * Test case to search when the specified destination does not exist.
   */
  @Test
  public void testCaseSearch_DoesNotExists() {
    String mockInput = System.lineSeparator() + "1" + System.lineSeparator() + "12" + System.lineSeparator() +
        "123" + System.lineSeparator() + "2" + System.lineSeparator() + "Bergen" + System.lineSeparator() + "Spikkestad" + System.lineSeparator() + "3";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseSearch(mockScanner);
  }

  /**
   * Test case to change departure details.
   */
  @Test
  public void testCaseChange() {
    String mockInput = System.lineSeparator() + "123" + System.lineSeparator() + "1" + System.lineSeparator() + "12:00"
        + System.lineSeparator() + "2" + System.lineSeparator() + "L2" + System.lineSeparator() + "3"
        + System.lineSeparator() + "Stavanger" + System.lineSeparator() + "4" + System.lineSeparator()
        + "2" + System.lineSeparator() + "5";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseChange(mockScanner);

    assertEquals(LocalTime.of(12, 0), trainDepartureRegister.getDepartureTime(123));
    assertEquals("L2",  trainDepartureRegister.getLine(123));
    assertEquals("Stavanger", trainDepartureRegister.getDestination(123));
    assertEquals("2", trainDepartureRegister.getTrack(123));
  }

  /**
   * Test case for negative change departure details.
   */
  @Test
  public void testChange_Negative() {
    String mockInput = System.lineSeparator() + "123" + System.lineSeparator() + "1" + System.lineSeparator() + "12:00"
        + System.lineSeparator() + "2" + System.lineSeparator() + "L2" + System.lineSeparator() + "3"
        + System.lineSeparator() + "Stavanger" + System.lineSeparator() + "4" + System.lineSeparator()
        + "2" + System.lineSeparator() + "5";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseChange(mockScanner);

    assertNotEquals(LocalTime.of(23, 16), trainDepartureRegister.getDepartureTime(123));
    assertNotEquals("L1",  trainDepartureRegister.getLine(123));
    assertNotEquals("Spikkestad", trainDepartureRegister.getDestination(123));
    assertNotEquals("4", trainDepartureRegister.getTrack(123));
  }

  /**
   * Test case to set current time.
   */
  @Test
  public void testCaseTime() {
    String mockInput = System.lineSeparator() + "10:00";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseTime(mockScanner);

    assertEquals(LocalTime.of(10, 0), userInterface.currentTime);
  }

  /**
   * Test case for negative time setting.
   */
  @Test
  public void testCaseTime_Negative() {
    String mockInput = System.lineSeparator() + "10:00";
    Scanner mockScanner = new Scanner(mockInput);

    userInterface.caseTime(mockScanner);

    assertFalse(LocalTime.of(0, 0).equals(userInterface.currentTime));
  }

  /**
   * Test case for testing data.
   */
  @Test
  public void testTestData() {
    userInterface.testData();

    assertEquals(5, trainDepartureRegister.getTrainDeparturesSize());
  }

  /**
   * Test case to check train numbers in test data.
   */
  @Test
  public void testTestData_TrainNumberCheck() {
    userInterface.testData();// The test data contains 5 train departures

    assertTrue(trainDepartureRegister.searchForTrainNumber(123));
    assertTrue(trainDepartureRegister.searchForTrainNumber(124));
    assertTrue(trainDepartureRegister.searchForTrainNumber(125));
    assertTrue(trainDepartureRegister.searchForTrainNumber(126));
    assertTrue(trainDepartureRegister.searchForTrainNumber(127));
  }

  /**
   * Test case for negative train number check in test data.
   */
  @Test
  public void testTestData_TrainNumberCheck_Negative() {
    userInterface.testData();

    assertFalse(trainDepartureRegister.searchForTrainNumber(122));
  }
}