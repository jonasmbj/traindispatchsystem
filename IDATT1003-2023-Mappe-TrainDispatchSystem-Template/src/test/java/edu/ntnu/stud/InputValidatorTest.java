package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test class for validating InputValidator methods.
 */
public class InputValidatorTest {

  /**
   * Positive test: checks if isInteger method returns true for a valid integer input.
   */
  @Test
  public void testIsInteger_ValidInteger_ReturnsTrue() {
    int validInteger = 42;

    assertTrue(InputValidator.isInteger(validInteger));
  }

  // Negative test scenarios

  /**
   * Positive test: checks if isTime method returns true for a valid time input.
   */
  @Test
  public void testIsTime_ValidTime_ReturnsTrue() {
    String validTime = "12:34";

    assertTrue(InputValidator.isTime(validTime));
  }

  /**
   * Negative test: checks if isTime method returns false for an invalid time input.
   */
  @Test
  public void testIsTime_ValidTime_ReturnsFalse() {
    String validTime = "25:75";

    assertFalse(InputValidator.isTime(validTime));
  }

  /**
   * Negative test: checks if isTime method returns false for a time input with letters.
   */
  @Test
  public void testIsTime_ValidTime_ReturnsFalseLetters() {
    String validTime = "ab:cd";

    assertFalse(InputValidator.isTime(validTime));
  }

  /**
   * Positive test: checks if isTrack method returns true for a valid track number input.
   */
  @Test
  public void testIsTrack_ValidTrack_ReturnsTrue() {
    String validTrack = "42";

    assertTrue(InputValidator.isTrack(validTrack));
  }

  /**
   * Negative test: checks if isTrack method returns false for an invalid track number input.
   */
  @Test
  public void testIsTrack_ValidTrack_ReturnsFalse() {
    String validTrack = "1000000";

    assertFalse(InputValidator.isTrack(validTrack));
  }

  /**
   * Negative test: checks if isTrack method returns false for a track number input with letters.
   */
  @Test
  public void testIsTrack_ValidTrack_ReturnsFalseLetters() {
    String validTrack = "abc";

    assertFalse(InputValidator.isTrack(validTrack));
  }
}

