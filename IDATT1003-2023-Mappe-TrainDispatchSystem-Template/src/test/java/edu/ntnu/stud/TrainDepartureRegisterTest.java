package edu.ntnu.stud;

import java.util.Scanner;
import org.junit.jupiter.api.Test;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test in this class have taken inspiration from a test made by GitHub Copilot:
 */
public class TrainDepartureRegisterTest {
  TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();

  /**
   * Test case to verify the functionality of adding a new train departure to the TrainDepartureRegister.
   */
  @Test
  public void testAddTrainDeparture_NewDepartureAdded() {
    // Arrange: Create a TrainDepartureRegister

    // Act: Add a new train departure
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(10, 30), "L1", 111, "Destination A", "1",
        LocalTime.of(0, 15)
    );

    // Assert: Verify the departure is added
    assertEquals(1, trainDepartureRegister.getTrainDeparturesSize());
    assertTrue(trainDepartureRegister.searchForTrainNumber(111));
  }

  /**
   * Test case to ensure that a departure with an invalid track number is not added to the TrainDepartureRegister.
   */
  @Test
  public void testAddTrainDeparture_InvalidTrackNumber_NotAdded() {
    // Arrange: Create a TrainDepartureRegister

    // Act: Attempt to add a departure with an invalid track number
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(9, 0), "Line X", 543, "Destination X",
        "InvalidTrack", LocalTime.of(0, 0)
    );

    // Assert: Verify that the departure is not added due to an invalid track number
    assertEquals(0, trainDepartureRegister.getTrainDeparturesSize());
    assertFalse(trainDepartureRegister.searchForTrainNumber(543));
  }

  /**
   * Positive test for printing all train departures before and after the current time.

   * Adding train departures, setting a current time, and printing
   *        all train departures before and after the current time.
   */
  @Test
  public void testPrintAllDepartures_BeforeAndAfterCurrentTime() {//Positive test, with time before and after current time
    // Arrange: Create a TrainDepartureRegister and add some TrainDeparture objects
    trainDepartureRegister.addTrainDeparture((LocalTime.of(15, 0)),
        "Line A", 123, "Station X", "1", LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture((LocalTime.of(19, 30)),
        "Line B", 456, "Station Y", "", LocalTime.of(0, 0));

    LocalTime currentTime = LocalTime.of(16, 0);
    // Act: Call printAllTrainDepartures method
    trainDepartureRegister.printAllTrainDepartures(currentTime);

    // Assert: Check if the method prints existing train departures
    // (Here, you may need to check the output in the console or verify with a custom output capturing mechanism)
    // Example: assertTrue(consoleOutput.contains("Line A")); - using a custom console output capture
  }

  /**
   * Test case to verify the behavior of printing all train departures when there are no departures registered.
   * This test aims to validate the expected output message when no train departures are present in the register.
   */
  @Test
  public void testPrintAllDepartures_NoDepartures() {//Negative test, with no departures
    // Arrange: Create a TrainDepartureRegister without adding any TrainDeparture objects

    LocalTime currentTime = LocalTime.of(0, 0);
    // Act: Call printAllTrainDepartures method
    trainDepartureRegister.printAllTrainDepartures(currentTime);

    // Assert: Check if the method prints "No train departures are registered"
    // (Here, you may need to check the output in the console or verify with a custom output capturing mechanism)
    // Example: assertTrue(consoleOutput.contains("No train departures are registered")); - using a custom console output capture
  }

  /**
   * Test case to validate the deletion of an existing train departure from the TrainDepartureRegister.
   * This positive test ensures that a departure, already existing in the register, is successfully deleted.
   */
  @Test
  public void testDeleteTrainDeparture_ExistingDepartureDeleted() {//Positive test
    // Arrange: Create a TrainDepartureRegister and add a departure
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(9, 0), "Line X", 543, "Destination X", "2",
        LocalTime.of(0, 0)
    );

    // Act: Delete the existing departure
    trainDepartureRegister.deleteTrainDeparture(543);

    // Assert: Verify that the departure is removed
    assertEquals(0, trainDepartureRegister.getTrainDeparturesSize());
    assertFalse(trainDepartureRegister.searchForTrainNumber(543));
  }

  /**
   * Test case to verify the behavior when attempting to delete a non-existing train departure from the TrainDepartureRegister.
   * This negative test ensures that attempting to delete a departure that does not exist in the register does not affect the register's content.
   */
  @Test
  public void testDeleteTrainDeparture_NonExistingDeparture_NotDeleted() {//Negative test
    // Arrange: Create a TrainDepartureRegister and add a departure
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(9, 0), "Line X", 543, "Destination X", "2",
        LocalTime.of(0, 0)
    );

    // Act: Attempt to delete a non-existing departure
    trainDepartureRegister.deleteTrainDeparture(123);

    // Assert: Verify that the departure is not removed
    assertEquals(1, trainDepartureRegister.getTrainDeparturesSize());
    assertTrue(trainDepartureRegister.searchForTrainNumber(543));
  }

  /**
   * Test case to validate the addition of a delay for a train departure with a valid train number.
   * This positive test verifies the functionality of adding a delay to an existing train departure.
   */
  @Test
  public void testAddDelay_ValidTrainNumber() {//Positive test
    // Arrange: Create a TrainDepartureRegister and add a departure for testing
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(9, 0), "Line X", 543, "Destination X", "2",
        LocalTime.of(0, 0)
    );
    // Simulate user input for a valid delay time
    String mockInput = "10:30"; // Valid delay time
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Add a delay for an existing train number with valid delay time
    trainDepartureRegister.addDelay(543, mockedScanner);

    // Assert: Verify that the delay is added
    // Use assertions to confirm that the delay was set for the specific train number
    LocalTime expectedDelay = LocalTime.of(10, 30);
      assertEquals(expectedDelay, trainDepartureRegister.getDelay(543));
  }

  /**
   * Test case to verify the behavior when attempting to add a delay to an invalid train number in the TrainDepartureRegister.
   * This negative test ensures that adding a delay to an invalid train number does not affect the specified train's departure schedule.
   */
  @Test
  public void testAddDelay_InvalidTrainNumber() {//Negative test
    // Arrange: Create a TrainDepartureRegister and add a departure for testing
    trainDepartureRegister.addTrainDeparture(
        LocalTime.of(9, 0), "Line X", 543, "Destination X", "2",
        LocalTime.of(0, 0)
    );
    // Simulate user input for a valid delay time
    String mockInput = "10:30"; // Valid delay time
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Add a delay for an existing train number with valid delay time
    trainDepartureRegister.addDelay(543, mockedScanner);

    // Assert: Verify that the delay is added
    // Use assertions to confirm that the delay was set for the specific train number
    LocalTime expectedDelay = LocalTime.of(0, 0);
    assertNotEquals(expectedDelay, trainDepartureRegister.getDelay(543));
  }

  /**
   * Test case to validate the search functionality for an existing train number in the TrainDepartureRegister.
   * This positive test ensures that the searchForTrainNumber method correctly returns true for an existing train number.
   */
  @Test
  public void testSearchForTrainNumber_ExistingTrainNumber_ReturnsTrue() {//Positive test
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture(LocalTime.of(8, 0), "Line A", 123,
        "Station X", "1", LocalTime.of(0, 0));

    // Act & Assert: Check if the searchForTrainNumber method returns true for an existing train number
    assertTrue(trainDepartureRegister.searchForTrainNumber(123));
  }

  /**
   * Test case to verify the search functionality for a non-existing train number in the TrainDepartureRegister.
   * This negative test ensures that the searchForTrainNumber method correctly returns false for a non-existing train number.
   */
  @Test
  public void testSearchForTrainNumber_NonExistingTrainNumber_ReturnsFalse() {//Negative test
    // Arrange: Create a TrainDepartureRegister without adding any TrainDeparture objects

    // Act & Assert: Check if the searchForTrainNumber method returns false for a non-existing train number
    assertFalse(trainDepartureRegister.searchForTrainNumber(99999));
  }

  /**
   * Test case to validate the search functionality for an existing destination in the TrainDepartureRegister.
   * This positive test ensures that the searchForDestination method correctly returns true for an existing destination.
   */
  @Test
  public void testSearchForDestination_ExistingDestination() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific destination
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 0,
        "Station X", "1", LocalTime.of(0, 0));

    // Act & Assert: Check if the searchForDestination method returns true for an existing destination
    assertTrue(trainDepartureRegister.searchForDestination("Station X"));
  }

  /**
   * Test case to verify the search functionality for a non-existing destination in the TrainDepartureRegister.
   * This negative test ensures that the searchForDestination method correctly returns false for a non-existing destination.
   */
  @Test
  public void testSearchForDestination_NonExistingDestination() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific destination
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 0,
        "Station X", "1", LocalTime.of(0, 0));

    // Act & Assert: Check if the searchForDestination method returns true for an existing destination
    assertFalse(trainDepartureRegister.searchForDestination("Station Y"));
  }

  /**
   * Test case to validate the functionality of changing the departure time for a train with a valid input.
   * This positive test ensures that the changeDepartureTime method effectively modifies the departure time for an existing train.
   */
  @Test
  public void testChangeDepartureTime_ValidInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for a valid departure time
    String mockInput = "10:30"; // Valid departure time
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Change the departure time for an existing train number with valid departure time
    trainDepartureRegister.changeDepartureTime(543, mockedScanner);

    // Assert: Verify that the departure time is changed
    // Use assertions to confirm that the departure time was changed for the specific train number
    LocalTime expectedDepartureTime = LocalTime.of(10, 30);
    assertEquals(expectedDepartureTime, trainDepartureRegister.getDepartureTime(543));
  }

  /**
   * Test case to verify the behavior of changing the departure time for a train with an invalid input.
   * This negative test ensures that the changeDepartureTime method does not modify the departure time
   *        for an existing train when provided with an invalid input.
   */
  @Test
  public void testChangeDepartureTime_NotOriginalInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for an invalid departure time
    String mockInput = "12:00"; // Invalid departure time
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Attempt to change the departure time for an existing train number with invalid departure time
    trainDepartureRegister.changeDepartureTime(543, mockedScanner);

    // Assert: Verify that the departure time is not changed
    // Use assertions to confirm that the departure time was not changed for the specific train number
    LocalTime OriginalDepartureTime = LocalTime.of(0, 0);
    assertNotEquals(OriginalDepartureTime, trainDepartureRegister.getDepartureTime(543));
  }

  /**
   * Test case to validate the functionality of changing the line for a train with a valid input.
   * This positive test ensures that the changeLine method effectively modifies the line for an existing train.
   */
  @Test
  public void testChangeLine_ValidInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for a valid line
    String mockInput = "L2"; // Valid line
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Change the line for an existing train number with valid line
    trainDepartureRegister.changeLine(543, mockedScanner);

    // Assert: Verify that the line is changed
    // Use assertions to confirm that the line was changed for the specific train number
    assertEquals("L2", trainDepartureRegister.getLine(543));
  }

  /**
   * Test case to verify the behavior of changing the line for a train with an invalid input.
   * This negative test ensures that the changeLine method does not modify the line
   *        for an existing train when provided with an invalid input.
   */
  @Test
  public void testChangeLine_NotOriginalInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for an invalid line
    String mockInput = "L2";
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Attempt to change the line for an existing train number with invalid line
    trainDepartureRegister.changeLine(543, mockedScanner);

    // Assert: Verify that the line is not changed
    // Use assertions to confirm that the line was not changed for the specific train number
    assertNotEquals("L1", trainDepartureRegister.getLine(543));
  }

  /**
   * Test case to validate the functionality of changing the destination for a train with a valid input.
   * This positive test ensures that the changeDestination method effectively modifies the destination for an existing train.
   */
  @Test
  public void testChangeDestination_ValidInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for an invalid line
    String mockInput = "Stavanger";
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Attempt to change the line for an existing train number with invalid line
    trainDepartureRegister.changeDestination(543, mockedScanner);

    // Assert: Verify that the line is not changed
    // Use assertions to confirm that the line was not changed for the specific train number
    assertEquals("Stavanger", trainDepartureRegister.getDestination(543));
  }

  /**
   * Test case to verify the behavior of changing the destination for a train with an invalid input.
   * This negative test ensures that the changeDestination method does not modify the destination
   *        for an existing train when provided with an invalid input.
   */
  @Test
  public void testChangeDestination_NotOriginalInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for an invalid line
    String mockInput = "Stavanger";
    Scanner mockedScanner = new Scanner(mockInput);


      // Act: Attempt to change the line for an existing train number with invalid line
      trainDepartureRegister.changeDestination(543, mockedScanner);

    // Assert: Verify that the line is not changed
    // Use assertions to confirm that the line was not changed for the specific train number
    assertNotEquals("Station X", trainDepartureRegister.getDestination(543));
  }

  /**
   * Test case to validate the functionality of changing the track number for a train with a valid input.
   * This positive test ensures that the changeTracks method effectively modifies the track number for an existing train.
   */
  @Test
  public void testChangeTrack_ValidInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for a valid track number
    String mockInput = "2"; // Valid track number
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Change the track number for an existing train number with valid track number
    trainDepartureRegister.changeTracks(543, mockedScanner);

    // Assert: Verify that the track number is changed
    // Use assertions to confirm that the track number was changed for the specific train number
    assertEquals("2", trainDepartureRegister.getTrack(543));
  }

  /**
   * Test case to verify the behavior of changing the track number for a train with an invalid input.
   * This negative test ensures that the changeTracks method does not modify the track number
   *        for an existing train when provided with an invalid input.
   */
  @Test
  public void testChangeTrack_InvalidInput() {
    // Arrange: Create a TrainDepartureRegister and add a TrainDeparture with a specific train number
    trainDepartureRegister.addTrainDeparture((LocalTime.of(0,0)), "L1", 543,
        "Station X", "1", LocalTime.of(0, 0));

    // Simulate user input for an invalid track number
    String mockInput = "Invalid"; // Invalid track number
    Scanner mockedScanner = new Scanner(mockInput);

    // Act: Attempt to change the track number for an existing train number with invalid track number
    trainDepartureRegister.changeTracks(543, mockedScanner);

    // Assert: Verify that the track number is not changed
    // Use assertions to confirm that the track number was not changed for the specific train number
    assertEquals("1", trainDepartureRegister.getTrack(543));
  }
}
